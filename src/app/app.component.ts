import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f') signupForm: NgForm;
  defaultSubs = 'Advanced';
  subs = ['Basic','Advanced','Pro'];

  user = {
    email: '',
    subs: ''
  };
  submitted = false;

  onSubmit() {
    this.submitted = true;
    this.user.email = this.signupForm.value.email;
    this.user.subs = this.signupForm.value.subs;
    console.log(this.signupForm.value);
    this.signupForm.reset();
  }
}
